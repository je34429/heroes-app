import React, { useContext } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';
import { types } from '../../types/types';

export const Navbar = () => {
    
    const navigate = useNavigate();
    const {user:{name}, dispatch} = useContext(AuthContext);

    const handleLogout = () => {
        //dispatch del logout
        const action = {
            type: types.logout
        }
        dispatch(action);
        
        navigate('/login',{
            replace:true
        });
    }

    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <div className="container px-5">
                <Link 
                    className="navbar-brand" 
                    to="/marvel"
                >
                    Asociaciones
                </Link>
            
                <div className="navbar-collapse">
                    <div className="navbar-nav align-items-center">

                        <NavLink 
                            className={({isActive})=> 'nav-item nav-link' + (isActive?'active':'')} 
                            to="/marvel"
                        >
                            Marvel
                        </NavLink>

                        <NavLink 
                            className={({isActive})=> 'nav-item nav-link' + (isActive?'active':'')}  
                            to="/dc"
                        >
                            DC
                        </NavLink>
                        <NavLink 
                            className={({isActive})=> 'nav-item nav-link' + (isActive?'active':'')}  
                            to="/search"
                        >
                            Search a hero
                        </NavLink>
                    </div>
                </div>

                <div className="navbar-collapse collapse w-90 order-3 dual-collapse2">
                    <ul className="navbar-nav ml-auto">
                        <span className="nav-item nav-link text-info">{name}</span>
                        <button 
                            className="nav-item nav-link btn" 
                            onClick={handleLogout}
                        >
                            Logout
                        </button>
                    </ul>
                </div>
            </div>
        </nav>
    )
}