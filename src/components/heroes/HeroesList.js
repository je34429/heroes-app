import React, { useMemo } from 'react'
import { getHeroByPublisher } from '../../selectors/getHeroByPublisher'
import { HeroCard } from './HeroCard';

export const HeroesList = ({publisher}) => {
    //excelente ejemplo para use memo, solo rerenderice el componente si el publisher cambia
    const heroes = useMemo(() => getHeroByPublisher(publisher), [publisher]);
    // const heroes = getHeroByPublisher(publisher);
    return (
        <div className="card-columns animate__animated animate__fadeIn">
            {
                heroes.map( hero => (
                    <HeroCard key={ hero.id }
                        {...hero}
                    />
                ))
            }
        </div>
    )
}
