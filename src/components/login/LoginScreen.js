import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';
import { types } from '../../types/types';

export const LoginScreen = () => {
    const context = useContext(AuthContext);
    const {dispatch} = context;
    console.log(context);
    const navigate = useNavigate();
    const handleLogin = () => {
        
        const lastPath = localStorage.getItem('lastPath')||'/marvel';

        const action ={
            type: types.login,
            payload:{
                name:'Jaime'
            }
        }
        dispatch(action);
        navigate(lastPath,{
            replace:true
        });
    } 

    return (
        <div className="container p-5">
            <h1>Login</h1>
            <hr/>
            <button
                className="btn btn-primary"
                onClick={handleLogin}
            >
                Login
            </button>
        </div>
    )
};
