import React, { useContext } from 'react'
import { LoginScreen } from '../components/login/LoginScreen';
import {
    BrowserRouter,
    Routes, Route
  } from "react-router-dom";
import { DashboardRoutes } from './DashboardRoutes';
import { AuthContext } from '../auth/AuthContext';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';




export const AppRouter = () => {
    const {user} = useContext(AuthContext);

    return (
        <BrowserRouter>
            <Routes>
   
                <Route path="/login" element={ 
                    <PublicRoute>
                       <LoginScreen/>
                    </PublicRoute> }
                />

                <Route path="/*" element={ 
                    <PrivateRoute>
                        <DashboardRoutes/>
                    </PrivateRoute> }
                />
            </Routes>
        </BrowserRouter>
    )

    
}

